package np.com.sagardevkota.lavamapstask.models.matrix;

import java.util.ArrayList;

/**
 * Created by HP on 8/9/2016.
 */
public class Row {
    ArrayList<Element> elements;

    public ArrayList<Element> getElements() {
        return elements;
    }

    public Row setElements(ArrayList<Element> elements) {
        this.elements = elements;
        return this;
    }
}
