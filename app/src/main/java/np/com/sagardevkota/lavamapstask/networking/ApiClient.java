package np.com.sagardevkota.lavamapstask.networking;

/**
 * Created by Dell on 8/9/2016.
 * Retrofit ApiClient Class
 */


import java.util.HashMap;
import java.util.Map;

import np.com.sagardevkota.lavamapstask.common.Const;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {


    private static Retrofit retrofit = null;

    /**
     * Returns singleton retrofit instance
     * @return
     */
    public static Retrofit getPlaceApiClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Const.BASE_URL_PLACES)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    /**
     * Default request parameters for place api
     * @return
     */
    public static Map<String,String> getDefaultPlaceRequestParams(){
        Map<String,String> placeRequestParams=new HashMap<>();
        placeRequestParams.put("radius", String.valueOf(Const.DEFAULT_RADIUS));
        placeRequestParams.put("sensor", "false");
        placeRequestParams.put("key", Const.PLACE_API_KEY);
        placeRequestParams.put("types", "school|university|bank|atm|hospital|cafe|embassy|shopping_mall|store");

        return placeRequestParams;

    }

    /**
     * Default request parameters for distancematrix api
     * @return
     */
    public static Map<String,String> getDefaultMatrixRequestParams(){
        Map<String,String> placeRequestParams=new HashMap<>();
        placeRequestParams.put("units", "imperial");
        placeRequestParams.put("key", Const.PLACE_API_KEY);

        return placeRequestParams;

    }
}