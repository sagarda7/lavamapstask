package np.com.sagardevkota.lavamapstask.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.JsonObject;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import np.com.sagardevkota.lavamapstask.R;
import np.com.sagardevkota.lavamapstask.models.place.Place;

/**
 * Fragment that shows map for selected destinbations
 * @author Sagar Devkota
 * @since 1.0
 */

public class MapViewFragment extends Fragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    public static final String ARG_SECTION_NUMBER = "section_number";
    private static final String TAG ="MapFragment" ;
    private String category;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleMap.CancelableCallback callback;
    private Context mContext;
    private boolean populated=false;
    private boolean initial=false;
    Marker curMarker;
    LocationListener mListener;


    public MapViewFragment() {
        // Empty constructor required for fragment subclasses
    }

    public static MapViewFragment newInstance(Context context,String section) {
        MapViewFragment fragment = new MapViewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SECTION_NUMBER, section);
        fragment.setArguments(args);
        fragment.mContext=context;

        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        category = getArguments().getString(ARG_SECTION_NUMBER);
        return rootView;
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        initial=true;
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        mGoogleApiClient.connect();

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG,"onPause Map fragment");
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    private void setUpMapIfNeeded() {

        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            Log.d(TAG,"class name is "+getActivity().getClass().getSimpleName());
            mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        callback=new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                Log.d(TAG,"hello");
            }

            @Override
            public void onCancel() {
                Log.d(TAG,"hello");
            }
        };


        mMap.setMyLocationEnabled(true);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return false;
            }
        });

    }

    @Override
    public void onConnected(Bundle bundle) {

        Log.i(TAG, "Location services connected.");
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        initial=true;
        startLocationUpdates(); //write here if not continious update required
        if (location == null) {

        } else {

           onLocationChanged(location);
        }

    }





    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if(initial==true){
            CameraPosition cameraPosition = new CameraPosition.Builder().
                    target(new LatLng(location.getLatitude(), location.getLongitude())).
                    tilt(0).
                    zoom(11).
                    bearing(0).
                    build();

            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), callback);
            initial=false;
        }
        if(mListener!=null)
            mListener.onLocationUpdated(new LatLng(location.getLatitude(),location.getLongitude()));



    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }

    }

    /**
     * Method to check if google play service is available or not
     * @return
     */
    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }

    protected void startLocationUpdates() {
        PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        Log.d(TAG, "Location update started ..............: ");
    }

    public void setOnLocationListener(LocationListener listener){
        this.mListener = listener;
    }

    public void updateMarkers(List<Place> items){
        if(mMap!=null){
            mMap.clear();
            for(Place item:items){

                LatLng location = new LatLng(item.getGeometry().getLocation().getLat(),item.getGeometry().getLocation().getLon());
                //Log.d("sagarda7",location.latitude+","+location.longitude);
                MarkerOptions markerOptions=new MarkerOptions()
                        .position(location)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_location_plain))
                        .title(item.getName())
                        .visible(true)
                        .anchor(0.5f,0.5f);

                mMap.addMarker(markerOptions);

            }
        }
    }


    public interface LocationListener{
        public void onLocationUpdated(LatLng location);
    }

}