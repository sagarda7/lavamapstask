package np.com.sagardevkota.lavamapstask.adapters;

/**
 * Created by Dell on 4/24/2015.
 * Adapter to show pagers for map view and list view
 */

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import np.com.sagardevkota.lavamapstask.customviews.NoSwipeViewPager;


public class PlaceFragmentPagerAdapter extends FragmentPagerAdapter{
    NoSwipeViewPager pager;
    Context mContext;
    List<Fragment> mItems;


    public PlaceFragmentPagerAdapter(FragmentManager fm,Context context, List<Fragment> items) {
        super(fm);
        mItems=items;
        mContext=context;
    }

    @Override
    public Fragment getItem(int position)
    {
        return mItems.get(position);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mItems.size();
    }

    @Override
    public CharSequence getPageTitle(int position){
        String title = "";
        switch(position){
            case 0:
                title = "List";
                break;
            case 1:
                title = "Map";
                break;

        }
        return title;
    }



}