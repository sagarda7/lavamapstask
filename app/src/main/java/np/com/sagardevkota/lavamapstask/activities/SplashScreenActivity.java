package np.com.sagardevkota.lavamapstask.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

import np.com.sagardevkota.lavamapstask.R;
import np.com.sagardevkota.lavamapstask.util.ConnectionDetector;

/**
 * SplashActivity.java, Shows Splash Screen
 * @author Sagar Devkota<sagarda7@yahoo.com>
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashScreenActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        waitAndOpen();
    }

    /**
     * waits for 3 second and calls checkConnection
     * @since 1.0
     */
    void waitAndOpen(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkConnection();
            }
        },3000);
    }

    /**
     * checks if network is connected or not, if not redirect to wifi setting
     * @since 1.0
     */
    void checkConnection(){
        if (!new ConnectionDetector(this).isConnectingToInternet()) {

            final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

            // Setting Dialog Title
            alertDialog.setTitle("ERROR");
            alertDialog.setMessage("Please connect to network and enable gps for best experience");

            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,"OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                            finish();

                        }
                    });

            // Showing Alert Message
            alertDialog.show();


            // Showing Alert Message
            alertDialog.show();
            return;
        }
        else
        {
            Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }





}
