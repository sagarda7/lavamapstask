package np.com.sagardevkota.lavamapstask.activities;
/**
 * BestLocationActivity.java, This activity shows best locations
 * @since 1.0
 * @author Sagar Devkota<sagarda7@yahoo.com>
 */
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import np.com.sagardevkota.lavamapstask.R;
import np.com.sagardevkota.lavamapstask.adapters.BestPlaceListAdapter;
import np.com.sagardevkota.lavamapstask.adapters.SortOptionAdapter;
import np.com.sagardevkota.lavamapstask.models.SortOption;
import np.com.sagardevkota.lavamapstask.models.matrix.MatrixResponse;
import np.com.sagardevkota.lavamapstask.models.matrix.Row;
import np.com.sagardevkota.lavamapstask.models.place.Place;
import np.com.sagardevkota.lavamapstask.models.place.PlaceResponse;
import np.com.sagardevkota.lavamapstask.networking.ApiClient;
import np.com.sagardevkota.lavamapstask.networking.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BestLocationActivity extends AppCompatActivity {

    private String TAG="BestLocationActivity";
    ArrayList<Place> destinations;
    LatLng origins;
    int index;
    SortOptionAdapter adapter;
    BestPlaceListAdapter bestPlaceListAdapter;
    List<SortOption> sortItems;

    private Handler handler = new Handler();
    boolean refresh=false;
    ApiInterface apiService;

    @BindView(R.id.txt_best_place_sort_info) TextView txtSortInfo;
    @BindView(R.id.swipe_refresh_best_loaction) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_view_best_places) RecyclerView mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_best_location);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle extras=getIntent().getExtras();
        origins=extras.getParcelable("origins");
        destinations=extras.getParcelableArrayList("destinations");

        apiService =
                ApiClient.getPlaceApiClient().create(ApiInterface.class);

        sortItems=new ArrayList<>();

        SortOption all=new SortOption();
        all.setChecked(true);
        all.setName("By Distance & Time");
        all.setIcon(R.drawable.ic_filter_tilt_shift_grey600_18dp);

        SortOption distance=new SortOption();
        distance.setChecked(false);
        distance.setName("By Distance");
        distance.setIcon(R.drawable.ic_directions_grey600_18dp);

        SortOption time=new SortOption();
        time.setChecked(false);
        time.setName("By Time");
        time.setIcon(R.drawable.ic_access_alarm_grey600_18dp);

        SortOption publicRating=new SortOption();
        publicRating.setChecked(false);
        publicRating.setName("By Rating");
        publicRating.setIcon(R.drawable.ic_grade_grey600_18dp);

        sortItems.add(all);
        sortItems.add(distance);
        sortItems.add(time);
        sortItems.add(publicRating);

        //will implement rating later

        adapter=new SortOptionAdapter(this,sortItems);

        setRefreshing();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                populatePlaces();
            }
        });

    }

    /**
     * shows refreshing on SwipeRefreshLayout
     * @since 1.0
     */
    private void setRefreshing() {

        if(!swipeRefreshLayout.isRefreshing()){
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                    populatePlaces();
                }
            });
        }

    }

    /**
     * Gets place info using distance matrix using API call
     * @since 1.0
     */
    public void populatePlaces(){

        //Now form request
        Map<String,String> matrixRequestParams = ApiClient.getDefaultMatrixRequestParams();
        matrixRequestParams.put("origins", origins.latitude+","+origins.longitude);
        StringBuilder sb=new StringBuilder();

        int i=0;
        for(Place place:destinations){
            if(i>0){
                sb.append("|");
            }

            sb.append(place.getGeometry().getLocation().getLat()+","+place.getGeometry().getLocation().getLon());
            i++;
        }

        matrixRequestParams.put("destinations", sb.toString());

        //now lets make api cal
        Call<MatrixResponse> call = apiService.getDistanceMatrix(matrixRequestParams);
        call.enqueue(new Callback<MatrixResponse>() {
            @Override
            public void onResponse(Call<MatrixResponse>call, Response<MatrixResponse> response) {
                List<Row> rows = response.body().getRows();
                //List<String> destinationAddresses = response.body().getDestinationAddresses();
                for(index=0; index<rows.get(0).getElements().size(); index++){
                    destinations.get(index).setMatrixElement(rows.get(0).getElements().get(index));
                    Log.d(TAG, "Matrix Received: " + rows.get(0).getElements().get(index).getDistance().getText());
                }


                LinearLayoutManager llm = new LinearLayoutManager(BestLocationActivity.this);
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                mList.setLayoutManager(llm);

                bestPlaceListAdapter=new BestPlaceListAdapter(BestLocationActivity.this,destinations);
                bestPlaceListAdapter.sort(BestPlaceListAdapter.SortType.ALL);
                mList.setAdapter(bestPlaceListAdapter);
                swipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onFailure(Call<MatrixResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    @OnClick(R.id.btn_best_location_sort)
    public void onSortClicked(View v){
        final AlertDialog.Builder builder=new AlertDialog.Builder(this);
        AlertDialog dialog=builder.create();
        builder.setTitle("Sort By");
        builder.setAdapter(adapter,new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                for(SortOption itm:sortItems){
                    if(itm.equals(sortItems.get(which))){
                       itm.setChecked(true);
                    }
                    else{
                       itm.setChecked(false);
                    }

                    txtSortInfo.setText("Top Places "+sortItems.get(which).getName());
                    adapter.notifyDataSetChanged();

                    //now sort place list
                    if(which==0){
                        bestPlaceListAdapter.sort(BestPlaceListAdapter.SortType.ALL);
                    }
                    else if(which==1){
                        bestPlaceListAdapter.sort(BestPlaceListAdapter.SortType.DISTANCE);
                    }
                    else if(which==2){
                        bestPlaceListAdapter.sort(BestPlaceListAdapter.SortType.DURATION);
                    }
                    else
                    {
                        bestPlaceListAdapter.sort(BestPlaceListAdapter.SortType.RATING);
                    }
                    bestPlaceListAdapter.notifyDataSetChanged();

                }

            }
        });

        builder.show();
    }


}
