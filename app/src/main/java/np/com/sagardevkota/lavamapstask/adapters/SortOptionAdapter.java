package np.com.sagardevkota.lavamapstask.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


import np.com.sagardevkota.lavamapstask.R;
import np.com.sagardevkota.lavamapstask.models.SortOption;

/**
 * Created by Dell on 1/12/2016.
 * Adapter to show list for sort options
 * @author Sagar Devkota
 */
public class SortOptionAdapter extends BaseAdapter {

    private final List<SortOption> list;
    private final Context context;

    static class ViewHolder {
        protected TextView name;
        protected ImageView icon;
        protected ImageView checkbox;
    }

    public SortOptionAdapter(Context context, List<SortOption> list) {

        this.context = context;
        this.list = list;
    }



    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;

        if (convertView == null) {
            view=LayoutInflater.from(context).inflate(R.layout.sort_option_row, parent, false);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView) view.findViewById(R.id.txt_label_sort_option);
            viewHolder.icon = (ImageView) view.findViewById(R.id.img_icon_sort_option);
            viewHolder.checkbox = (ImageView) view.findViewById(R.id.img_icon_sort_check);
            view.setTag(viewHolder);
        } else {
            view = convertView;
        }

        ViewHolder holder = (ViewHolder) view.getTag();
        holder.name.setText(list.get(position).getName());
        holder.icon.setImageResource(list.get(position).getIcon());

        if(list.get(position).isChecked()){
            holder.checkbox.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.checkbox.setVisibility(View.GONE);
        }

        return view;
    }
}