package np.com.sagardevkota.lavamapstask.fragments;

/**
 * This fragment class is used for displaying place list to be selected
 * @author Sagar Devkota
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import np.com.sagardevkota.lavamapstask.R;
import np.com.sagardevkota.lavamapstask.activities.MainActivity;
import np.com.sagardevkota.lavamapstask.adapters.PlaceListAdapter;
import np.com.sagardevkota.lavamapstask.models.place.Place;
import np.com.sagardevkota.lavamapstask.models.place.PlaceResponse;
import np.com.sagardevkota.lavamapstask.networking.ApiClient;
import np.com.sagardevkota.lavamapstask.networking.ApiInterface;
import np.com.sagardevkota.lavamapstask.util.EndlessRecyclerViewBehaviour;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Fragment that appears in the "content_frame", shows a planet
 */
public class ListViewFragment extends Fragment implements PlaceListAdapter.PlaceListClickListener {
    public static final String ARG_SECTION_NUMBER = "section_number";

    RecyclerView lstView;


    private SwipeRefreshLayout swipeRefreshLayout;
    String category;
    static MainActivity parent;
    private String TAG=getClass().getSimpleName();
    View rootView;
    PlaceListAdapter placeListAdapter;
    private View loading;
    PlaceListener mListener;
    LatLng mCurrentLocation;
    ApiInterface apiService;
    EndlessRecyclerViewBehaviour endlessRecycleViewBehaviour;
    String nextPageToken;
    boolean firstPage=true;


    public ListViewFragment() {
        // Empty constructor required for fragment subclasses
    }

    public static ListViewFragment newInstance(Context context,String section) {
        ListViewFragment fragment = new ListViewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SECTION_NUMBER, section);
        fragment.setArguments(args);
        parent=(MainActivity) context;

        return fragment;
    }

    public PlaceListAdapter getDefaultAdapter(){
        return placeListAdapter;
    }

    public  void setCurrentLcation(LatLng currentLcation){
        mCurrentLocation=currentLcation;
        firstPage=true;
        populatePlaces(mCurrentLocation);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_list, container, false);
        category = getArguments().getString(ARG_SECTION_NUMBER);

        //get retrofit api client
        apiService =
                ApiClient.getPlaceApiClient().create(ApiInterface.class);

        lstView=(RecyclerView) rootView.findViewById(R.id.recycler_view_fragement_list_places);

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        lstView.setLayoutManager(llm);

        //make recyclerview view endless
        endlessRecycleViewBehaviour=new EndlessRecyclerViewBehaviour(lstView);
        endlessRecycleViewBehaviour.setOnLoadMoreListener(new EndlessRecyclerViewBehaviour.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.d(TAG,"endless occured");
                if(nextPageToken!=null){
                    populatePlaces(mCurrentLocation);
                }
            }
        });

        loading = rootView.findViewById(R.id.list_view_loading);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     * Populates places from google place api and adds to RecyclerView
     * @param location
     */
    void populatePlaces(LatLng location){
        showLoading();
        //Now form request
        Map<String,String> placeRequestParams = ApiClient.getDefaultPlaceRequestParams();
        placeRequestParams.put("location", location.latitude+","+location.longitude);
        if(nextPageToken!=null)
            placeRequestParams.put("pagetoken", nextPageToken);

        Call<PlaceResponse> call = apiService.getPlaces(placeRequestParams);
        call.enqueue(new Callback<PlaceResponse>() {
            @Override
            public void onResponse(Call<PlaceResponse>call, Response<PlaceResponse> response) {
                List<Place> places = response.body().getResults();
                Log.d(TAG, "Place Received: " + response.raw().request().url());
                nextPageToken = response.body().getNextPageToken();

                //if first page generate list otherwise append items to recyclerview
                if(firstPage)
                    updateList(places);
                else
                    appendList(places);

                //if nextpagetoken, so it is not first page
                if(nextPageToken!=null){
                    firstPage=false;
                }

                endlessRecycleViewBehaviour.loadingFinished();
                hideLoading();
            }

            @Override
            public void onFailure(Call<PlaceResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                hideLoading();
            }
        });
    }

    /**
     * Assigns items to new adapter and sets adapter for RecyclerView
     * @param items
     */
    public void updateList(List<Place> items){
        lstView.setVisibility(View.VISIBLE);
        loading.setVisibility(View.GONE);
        placeListAdapter = new PlaceListAdapter(getActivity().getApplicationContext(),items);
        placeListAdapter.setItemClickListener(this);
        lstView.setAdapter(placeListAdapter);
    }

    /**
     * When data is obtained using pageToken, appended
     * @param items
     */
    public void appendList(List<Place> items){
        loading.setVisibility(View.GONE);
        if(placeListAdapter!=null){
            placeListAdapter.appendItems(items);
        }
    }


    public void showLoading(){
        //lstView.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);

    }


    /**
     * Hides ProgressIndicator
     */
    public void hideLoading(){
        loading.setVisibility(View.GONE);
    }


    @Override
    public void onItemClick(View v) {

    }

    /**
     * When item is tapped
     */
    @Override
    public void onItemSelected(ArrayList<Place> items, int count) {
        this.mListener.onItemsSelectionUpdated(items);
    }

    public void setPlaceListener(PlaceListener listener){
        this.mListener = listener;
    }


    public interface PlaceListener{
        public void onItemsSelectionUpdated(ArrayList<Place> items);

    }


}