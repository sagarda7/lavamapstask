package np.com.sagardevkota.lavamapstask.models.place;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import np.com.sagardevkota.lavamapstask.models.matrix.Element;

/**
 * Created by Dell on 8/9/2016.
 */
public class Place implements Parcelable {
    private Geometry geometry;
    private String name;
    private Float rating;
    private ArrayList<String> types;
    private String vicinity;
    private Element matrixElement;

    private boolean selected=false;

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public ArrayList<String> getTypes() {
        return types;
    }

    public void setTypes(ArrayList<String> types) {
        this.types = types;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public boolean isSelected() {
        return selected;
    }

    public Place setSelected(boolean selected) {
        this.selected = selected;
        return this;
    }

    public Element getMatrixElement() {
        return matrixElement;
    }

    public void setMatrixElement(Element matrixElement) {
        this.matrixElement = matrixElement;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.geometry, flags);
        dest.writeString(this.name);
        dest.writeValue(this.rating);
        dest.writeStringList(this.types);
        dest.writeString(this.vicinity);
        dest.writeParcelable(this.matrixElement, flags);
        dest.writeByte(this.selected ? (byte) 1 : (byte) 0);
    }

    public Place() {
    }

    protected Place(Parcel in) {
        this.geometry = in.readParcelable(Geometry.class.getClassLoader());
        this.name = in.readString();
        this.rating = (Float) in.readValue(Float.class.getClassLoader());
        this.types = in.createStringArrayList();
        this.vicinity = in.readString();
        this.matrixElement = in.readParcelable(Element.class.getClassLoader());
        this.selected = in.readByte() != 0;
    }

    public static final Creator<Place> CREATOR = new Creator<Place>() {
        @Override
        public Place createFromParcel(Parcel source) {
            return new Place(source);
        }

        @Override
        public Place[] newArray(int size) {
            return new Place[size];
        }
    };
}
