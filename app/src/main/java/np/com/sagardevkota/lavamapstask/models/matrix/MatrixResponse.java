package np.com.sagardevkota.lavamapstask.models.matrix;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by HP on 8/9/2016.
 */
public class MatrixResponse {
    @SerializedName("destination_addresses")
    ArrayList<String> destinationAddresses;

    ArrayList<Row> rows;

    public ArrayList<String> getDestinationAddresses() {
        return destinationAddresses;
    }

    public MatrixResponse setDestinationAddresses(ArrayList<String> destinationAddresses) {
        this.destinationAddresses = destinationAddresses;
        return this;
    }

    public ArrayList<Row> getRows() {
        return rows;
    }

    public MatrixResponse setRows(ArrayList<Row> rows) {
        this.rows = rows;
        return this;
    }
}
