package np.com.sagardevkota.lavamapstask.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import np.com.sagardevkota.lavamapstask.R;
import np.com.sagardevkota.lavamapstask.models.place.Place;

/**
 * Created by HP on 8/19/2015.
 * Adapter for best places
 */

public class BestPlaceListAdapter extends RecyclerView.Adapter<BestPlaceListAdapter.ViewHolder> implements View.OnCreateContextMenuListener {
    private List<Place> mItems;
    private List<Place> selectedItems=new ArrayList<>();
    Context mContext;
    PlaceListClickListener mListener;

    public enum SortType {
        ALL,
        DISTANCE,
        DURATION,
        RATING
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public  class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        public Object item;

        @BindView(R.id.txt_best_place_name) TextView mName;
        @BindView(R.id.text_best_places_row_time) TextView mTime;
        @BindView(R.id.text_best_places_row_distance) TextView mDistance;
        @BindView(R.id.text_best_places_row_rating_value) TextView mRatingValue;
        @BindView(R.id.rating_best_places_row) RatingBar mRating;
        @BindView(R.id.layout_card_layout_home_container) RelativeLayout mContainer;
        public ViewHolder(View v) {
            super(v);

            ButterKnife.bind(this,v);

        }





    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public BestPlaceListAdapter(Context mContext, List<Place> mItems) {
        this.mItems = mItems;
        this.mContext=mContext;
        Log.d("sagar", "view holder==sss");

    }

    public void setItemClickListener(PlaceListClickListener listener){
        this.mListener=listener;

    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.best_places_row, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        v.setTag(vh);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Place item=mItems.get(position);
        holder.item=item;
        holder.mName.setText(item.getName());
        holder.mDistance.setText(item.getMatrixElement().getDistance().getKmValue()+" km");
        holder.mTime.setText(item.getMatrixElement().getDuration().getText());
        if(item.getRating()!=null) {
            holder.mRatingValue.setText(String.valueOf(item.getRating()));
            holder.mRating.setRating(item.getRating());
        }
        else
        {
            holder.mRatingValue.setText("NA");
            holder.mRating.setRating(0);
        }



        holder.mContainer.setTag(item);
        holder.mContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mListener.onItemClick(v);
                //commented later we will use this when needed to handle click events
            }
        });
    }

    /**
     * Sorts ArrayList using comparator interface
     * @param order
     */
    public void sort(final SortType order){
        Collections.sort(mItems, new Comparator<Place>() {
            @Override
            public int compare(Place o1, Place o2) {
                if(order==SortType.ALL) {
                    float dComp= o1.getMatrixElement().getDistance().getKmValue() - o2.getMatrixElement().getDistance().getKmValue();
                    //Log.d("sagarda77", o1.getMatrixElement().getDistance().getKmValue() +" "+ o2.getMatrixElement().getDistance().getKmValue());
                    if (dComp == 0) {
                        int x1 = o1.getMatrixElement().getDuration().getValue();
                        int x2 = o2.getMatrixElement().getDuration().getValue();
                        return x1-x2;
                    } else {
                        if(o1.getMatrixElement().getDistance().getKmValue() < o2.getMatrixElement().getDistance().getKmValue()){
                            return -1;
                        }
                        else if(o1.getMatrixElement().getDistance().getKmValue() > o2.getMatrixElement().getDistance().getKmValue()){
                            return 1;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                }
                else if(order==SortType.DISTANCE) {
                    if(o1.getMatrixElement().getDistance().getKmValue() < o2.getMatrixElement().getDistance().getKmValue()){
                        return -1;
                    }
                    else if(o1.getMatrixElement().getDistance().getKmValue() > o2.getMatrixElement().getDistance().getKmValue()){
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else if(order==SortType.DURATION)
                {
                    return o1.getMatrixElement().getDuration().getValue() - o2.getMatrixElement().getDuration().getValue();
                }
                else
                {
                    if(o2.getRating()==null){
                        o2.setRating(0f);
                    }
                    if(o1.getRating()==null){
                        o1.setRating(0f);
                    }

                    if(o2.getRating()>o1.getRating()){
                        return 1;
                    }
                    else if(o2.getRating()<o1.getRating())
                    {
                        return -1;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void deleteItem(Place item){
        mItems.remove(item);
    }

    /**
     * interface for Place List
     */
    public interface PlaceListClickListener{
        public void onItemClick(View v);
        public void onItemSelected(List<Place> item, int count);
    }


}