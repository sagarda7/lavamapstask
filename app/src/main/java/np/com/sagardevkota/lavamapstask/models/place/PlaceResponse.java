package np.com.sagardevkota.lavamapstask.models.place;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Dell on 8/9/2016.
 */
public class PlaceResponse {
    ArrayList<Place> results;

    @SerializedName("next_page_token")
    String nextPageToken;


    public ArrayList<Place> getResults() {
        return results;
    }

    public void setResults(ArrayList<Place> results) {
        this.results = results;
    }

    public String getNextPageToken() {
        return nextPageToken;
    }

    public void setNextPageToken(String nextPageToken) {
        this.nextPageToken = nextPageToken;
    }
}
