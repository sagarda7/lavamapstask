package np.com.sagardevkota.lavamapstask.models.matrix;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by HP on 8/9/2016.
 */
public class Duration implements Parcelable {
    String text;
    int value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getValue() {
        return value;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.text);
        dest.writeInt(this.value);
    }

    public Duration() {
    }

    protected Duration(Parcel in) {
        this.text = in.readString();
        this.value = in.readInt();
    }

    public static final Parcelable.Creator<Duration> CREATOR = new Parcelable.Creator<Duration>() {
        @Override
        public Duration createFromParcel(Parcel source) {
            return new Duration(source);
        }

        @Override
        public Duration[] newArray(int size) {
            return new Duration[size];
        }
    };
}
