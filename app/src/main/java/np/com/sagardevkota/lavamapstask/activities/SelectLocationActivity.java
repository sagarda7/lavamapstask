package np.com.sagardevkota.lavamapstask.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import np.com.sagardevkota.lavamapstask.R;
import np.com.sagardevkota.lavamapstask.adapters.PlaceListAdapter;
import np.com.sagardevkota.lavamapstask.fragments.ListViewFragment;
import np.com.sagardevkota.lavamapstask.models.place.Place;
import np.com.sagardevkota.lavamapstask.models.place.PlaceResponse;
import np.com.sagardevkota.lavamapstask.networking.ApiClient;
import np.com.sagardevkota.lavamapstask.networking.ApiInterface;
import np.com.sagardevkota.lavamapstask.util.EndlessRecyclerViewBehaviour;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectLocationActivity extends AppCompatActivity implements PlaceListAdapter.PlaceListClickListener{

    private String TAG=getClass().getSimpleName();
    PlaceListAdapter placeListAdapter;
    ListViewFragment.PlaceListener mListener;
    LatLng mCurrentLocation;
    ApiInterface apiService;
    EndlessRecyclerViewBehaviour endlessRecycleViewBehaviour;
    String nextPageToken;
    boolean firstPage=true;
    ArrayList<Place> selectedItems;
    MenuItem finish;

    @BindView(R.id.txt_total_places_info) TextView txtPlacesInfo;
    @BindView(R.id.progress_loading_select_places) ProgressBar footerLoadingView;
    @BindView(R.id.recycler_view_select_place) RecyclerView mList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_location);

        ButterKnife.bind(this);
        //mList.setNestedScrollingEnabled(true);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mList.setLayoutManager(llm);

        //get data
        Bundle extras=getIntent().getExtras();
        mCurrentLocation= (LatLng) extras.getParcelable("current_location");
        apiService =
                ApiClient.getPlaceApiClient().create(ApiInterface.class);

        //make recyclerview view endless
        endlessRecycleViewBehaviour=new EndlessRecyclerViewBehaviour(mList);
        endlessRecycleViewBehaviour.setOnLoadMoreListener(new EndlessRecyclerViewBehaviour.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.d(TAG,"endless occured");
                if(nextPageToken!=null){
                    populatePlaces(mCurrentLocation);
                }
            }
        });
        populatePlaces(mCurrentLocation);

    }

    /**
     * Populates places from google place api and adds to RecyclerView
     * @param location
     */
    void populatePlaces(LatLng location){
        showLoading();
        //Now form request
        Map<String,String> placeRequestParams = ApiClient.getDefaultPlaceRequestParams();
        placeRequestParams.put("location", location.latitude+","+location.longitude);
        if(nextPageToken!=null)
            placeRequestParams.put("pagetoken", nextPageToken);

        Call<PlaceResponse> call = apiService.getPlaces(placeRequestParams);
        call.enqueue(new Callback<PlaceResponse>() {
            @Override
            public void onResponse(Call<PlaceResponse>call, Response<PlaceResponse> response) {
                List<Place> places = response.body().getResults();
                Log.d(TAG, "Place Received: " + response.raw().request().url());
                nextPageToken = response.body().getNextPageToken();

                //if first page generate list otherwise append items to recyclerview
                if(firstPage)
                    updateList(places);
                else
                    appendList(places);

                //if nextpagetoken, so it is not first page
                if(nextPageToken!=null){
                    firstPage=false;
                }

                endlessRecycleViewBehaviour.loadingFinished();
                hideLoading();
            }

            @Override
            public void onFailure(Call<PlaceResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                hideLoading();
            }
        });
    }

    /**
     * Assigns items to new adapter and sets adapter for RecyclerView
     * @param items
     */
    public void updateList(List<Place> items){
        mList.setVisibility(View.VISIBLE);
        footerLoadingView.setVisibility(View.GONE);
        placeListAdapter = new PlaceListAdapter(this,items);
        placeListAdapter.setItemClickListener(this);
        mList.setAdapter(placeListAdapter);
    }

    /**
     * When data is obtained using pageToken, appended
     * @param items
     */
    public void appendList(List<Place> items){
        footerLoadingView.setVisibility(View.GONE);
        if(placeListAdapter!=null){
            placeListAdapter.appendItems(items);
        }
    }


    public void showLoading(){
        //lstView.setVisibility(View.GONE);
        footerLoadingView.setVisibility(View.VISIBLE);

    }


    /**
     * Hides ProgressIndicator
     */
    public void hideLoading(){
        footerLoadingView.setVisibility(View.GONE);
    }


    @Override
    public void onItemClick(View v) {

    }

    @Override
    public void onItemSelected(ArrayList<Place> item, int count) {
        txtPlacesInfo.setText(count+" Places Selected");
        selectedItems=item;
        if(count>0){
            finish.setVisible(true);
        }
        else
        {
            finish.setVisible(false);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_select_location, menu);
        finish = menu.findItem(R.id.action_finish);
        finish.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        switch (id){
            case R.id.action_finish:
                Intent intentMessage=new Intent();

                // put the message in Intent
                intentMessage.putParcelableArrayListExtra("LOCATIONS",selectedItems);
                // Set The Result in Intent
                setResult(1,intentMessage);
                finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
