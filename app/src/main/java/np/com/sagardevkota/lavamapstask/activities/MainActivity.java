package np.com.sagardevkota.lavamapstask.activities;
/**
 * MainActivity.java, This is main activity
 * @since 1.0
 * @author Sagar Devkota<sagarda7@yahoo.com>
 */

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Geocoder;
import android.os.Parcelable;
import android.provider.Contacts;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.identity.intents.Address;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import np.com.sagardevkota.lavamapstask.R;
import np.com.sagardevkota.lavamapstask.adapters.PlaceFragmentPagerAdapter;
import np.com.sagardevkota.lavamapstask.common.Const;
import np.com.sagardevkota.lavamapstask.customviews.CustomProgressDialog;
import np.com.sagardevkota.lavamapstask.customviews.NoSwipeViewPager;
import np.com.sagardevkota.lavamapstask.fragments.ListViewFragment;
import np.com.sagardevkota.lavamapstask.fragments.MapViewFragment;
import np.com.sagardevkota.lavamapstask.models.place.Place;
import np.com.sagardevkota.lavamapstask.util.ConnectionDetector;

public class MainActivity extends AppCompatActivity implements ListViewFragment.PlaceListener, MapViewFragment.LocationListener{

    private static final String TAG = "MainActivity" ;

    MapViewFragment mapViewFragment;
    ListViewFragment listViewFragment;
    FragmentManager fragmentManager;
    private PlaceFragmentPagerAdapter placePagerAdapter;
    LatLng currentLocation;
    boolean locationResolved=false;
    List<Place> destinations;
    ProgressDialog dialog;


    @BindView(R.id.btn_activity_main_view_type) ImageButton btnViewType;
    @BindView(R.id.place_pager) NoSwipeViewPager vpager;
    @BindView(R.id.txt_activity_main_start_address) TextView txtStartAddress;
    @BindView(R.id.txt_activity_main_destination_address) TextView txtDestinationAddress;
    @BindView(R.id.txt_activity_main_radius) TextView txtRadiusInfo;
    @BindView(R.id.seekbar_radius) SeekBar seekRadius;
    private boolean initial=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initComponents();


    }



    void initComponents(){
        btnViewType.setTag("list");

        listViewFragment=ListViewFragment.newInstance(MainActivity.this, "list");
        listViewFragment.setPlaceListener(this);

        mapViewFragment=MapViewFragment.newInstance(MainActivity.this, "map");
        mapViewFragment.setOnLocationListener(this);

        fragmentManager = getSupportFragmentManager();

        List<Fragment> items=new ArrayList<>();

        items.add(listViewFragment);
        items.add(mapViewFragment);

        placePagerAdapter = new PlaceFragmentPagerAdapter(getSupportFragmentManager(),this,items);
        final int curRadius=((Double)(seekRadius.getProgress()+0.5)).intValue()+10;
        Const.DEFAULT_RADIUS = curRadius*1000;
        txtRadiusInfo.setText("Search within "+curRadius+" km");

        seekRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                final int value=((Double)(seekRadius.getProgress()+0.5)).intValue()+10;
                Const.DEFAULT_RADIUS = value*1000;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txtRadiusInfo.setText("Search within "+value+" km");
                        if(locationResolved){
                            listViewFragment.setCurrentLcation(currentLocation);
                        }
                    }
                });

            }
        });


        // Binds the Adapter to the ViewPager
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                vpager.setAdapter(placePagerAdapter);
            }
        });

        dialog = new ProgressDialog(this);
        dialog.setTitle("Resolving Location...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(false);
        dialog.show();



    }

    @OnClick(R.id.btn_activity_main_view_type)
    public void onViewTypeClicked(View v){
        ImageView self= (ImageView) v;
        //lets set pagers based on button clicked
        /*if(self.getTag().equals("list")){
            vpager.setCurrentItem(1);
            self.setImageDrawable(getResources().getDrawable(R.drawable.ic_list_grey600_36dp));
            self.setTag("map");
        }
        else
        {
            vpager.setCurrentItem(0);
            self.setImageDrawable(getResources().getDrawable(R.drawable.ic_map_grey600_36dp));
            self.setTag("list");
        }*/

        Intent intent=new Intent(this,SelectLocationActivity.class);
        intent.putExtra("current_location", currentLocation);
        startActivityForResult(intent,1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==1){
            destinations = data.getParcelableArrayListExtra("LOCATIONS");
            txtDestinationAddress.setText("To "+destinations.size()+" Selected Places");

        }
    }

    @Override
    public void onItemsSelectionUpdated(ArrayList<Place> items) {
        txtDestinationAddress.setText("To "+items.size()+" Selected Places");
        destinations=items;
        mapViewFragment.updateMarkers(items);
    }

    @Override
    public void onLocationUpdated(LatLng location) {
        locationResolved=true;
        currentLocation=location;

        txtStartAddress.setText(location.latitude+","+location.longitude);
        if(initial==true) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    listViewFragment.setCurrentLcation(currentLocation);
                }
            });

            initial = false;
        }

        //get geo coded location
        Geocoder geocoder;
        List<android.location.Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(location.latitude, location.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

            txtStartAddress.setText("From "+knownName);
            dialog.dismiss();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @OnClick(R.id.btn_activity_main_calculate)
    public void onCalculateClicked(View v){
        //if location is resolved and destinations are set then open best location activity
        if(locationResolved && destinations!=null && destinations.size()>0){
            Intent intent=new Intent(this,BestLocationActivity.class);
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("destinations", (ArrayList<? extends Parcelable>) destinations);
            bundle.putParcelable("origins",currentLocation);

            intent.putExtras(bundle);
            startActivity(intent);
        }
        else
        {
            Toast.makeText(this,"Please make sure source and destinations are set",Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Shows my location  map
     * @since 1.0
     */
    @OnClick(R.id.btn_activity_main_source_map)
    public void onMyLocationButtonClicked(View v){
        if(locationResolved && currentLocation!=null){
            Intent intent=new Intent(this,MapActivity.class);
            Bundle bundle = new Bundle();
            bundle.putDouble("lat", currentLocation.latitude);
            bundle.putDouble("lon",currentLocation.longitude);

            intent.putExtras(bundle);
            startActivity(intent);
        }
        else
        {
            Toast.makeText(this,"Please make sure your location is resolved",Toast.LENGTH_LONG).show();
        }
    }
}
