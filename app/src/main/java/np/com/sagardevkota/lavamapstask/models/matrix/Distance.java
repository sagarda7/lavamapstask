package np.com.sagardevkota.lavamapstask.models.matrix;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by HP on 8/9/2016.
 */
public class Distance implements Parcelable {
    String text;
    int value;

    public String getText() {
        return text;
    }

    public Distance setText(String text) {
        this.text = text;
        return this;
    }

    public int getValue() {
        return value;
    }

    public float getKmValue() {
        float v= (value/1000.0f);
        return Math.round(v * 10.0f)/10.0f;
    }

    public Distance setValue(int value) {
        this.value = value;
        return this;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.text);
        dest.writeInt(this.value);
    }

    public Distance() {
    }

    protected Distance(Parcel in) {
        this.text = in.readString();
        this.value = in.readInt();
    }

    public static final Parcelable.Creator<Distance> CREATOR = new Parcelable.Creator<Distance>() {
        @Override
        public Distance createFromParcel(Parcel source) {
            return new Distance(source);
        }

        @Override
        public Distance[] newArray(int size) {
            return new Distance[size];
        }
    };
}
