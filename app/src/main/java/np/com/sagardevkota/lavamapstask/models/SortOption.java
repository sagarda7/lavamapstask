package np.com.sagardevkota.lavamapstask.models;

/**
 * Created by Dell on 8/10/2016.
 */
public class SortOption {
    int icon;
    String name;
    boolean checked;

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
