package np.com.sagardevkota.lavamapstask.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import np.com.sagardevkota.lavamapstask.R;
import np.com.sagardevkota.lavamapstask.models.place.Place;

/**
 * Created by HP on 8/19/2015.
 * Adapter to show place list in main screen
 * @author Sagar Devkota<sagarda7@yahoo.com>
 */

public class PlaceListAdapter extends RecyclerView.Adapter<PlaceListAdapter.ViewHolder> implements View.OnCreateContextMenuListener {
    private List<Place> mItems;
    private ArrayList<Place> selectedItems=new ArrayList<>();
    Context mContext;
    PlaceListClickListener mListener;

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public  class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public Object item;

        @BindView(R.id.txt_place_name) TextView mName;
        @BindView(R.id.rating_places_row) RatingBar mRating;
        @BindView(R.id.layout_card_layout_home_container) RelativeLayout mContainer;
        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
        }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public  class ProgressViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        @BindView(R.id.layout_card_layout_progress_container) RelativeLayout mContainer;
        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);

        }

    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public PlaceListAdapter(Context mContext, List<Place> mItems) {
        this.mItems = mItems;
        this.mContext=mContext;

    }

    public void setItemClickListener(PlaceListClickListener listener){
        this.mListener=listener;

    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.places_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh;
        vh= new ViewHolder(v);
        v.setTag(vh);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Place item=mItems.get(position);
        holder.item=item;
        holder.mName.setText(item.getName());
        if(item.getRating()!=null) {
            holder.mRating.setRating(item.getRating());
        }
        else {
            holder.mRating.setRating(0);
        }

        if(item.isSelected()){
            holder.mContainer.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        }
        else
        {
            holder.mContainer.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryLight));
        }


        holder.mContainer.setTag(item);
        //handle click on item
        holder.mContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(v);
                Place item = (Place) v.getTag();
                if(item.isSelected()){
                    item.setSelected(false);
                    selectedItems.remove(item);
                    mListener.onItemSelected(selectedItems, selectedItems.size());
                }
                else {

                    if (selectedItems.size() < 7) {

                        if (!item.isSelected()) {
                            item.setSelected(true);
                            selectedItems.add(item);
                            mListener.onItemSelected(selectedItems, selectedItems.size());
                        }

                    }

                }

                notifyDataSetChanged();
            }
        });
    }

    //Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mItems.size();
    }

    /**
     * deletes item from adapter
     * @param item
     */
    public void deleteItem(Place item){
        mItems.remove(item);
    }

    /**
     * Append item on list used by endless recycler view
     * @param items
     */
    public void appendItems(List<Place> items){
        mItems.addAll(items);
        notifyDataSetChanged();
    }

    public interface PlaceListClickListener{
        public void onItemClick(View v);
        public void onItemSelected(ArrayList<Place> item, int count);
    }


}