package np.com.sagardevkota.lavamapstask.networking;

/**
 * Created by Dell on 8/9/2016.
 */
import java.util.Map;

import np.com.sagardevkota.lavamapstask.common.Const;
import np.com.sagardevkota.lavamapstask.models.matrix.MatrixResponse;
import np.com.sagardevkota.lavamapstask.models.place.PlaceResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Interface which contains all endpoints to be called
 */
public interface ApiInterface {
    @GET("/maps/api/place/nearbysearch/json")
    public Call<PlaceResponse> getPlaces(@QueryMap Map<String,String> data);

    @GET("maps/api/distancematrix/json")
    public Call<MatrixResponse> getDistanceMatrix(@QueryMap Map<String,String> data);

    @GET("maps/api/distancematrix/json")
    public Call<ResponseBody> getDistanceMatrixAsResponseBody(@QueryMap Map<String,String> data);
}