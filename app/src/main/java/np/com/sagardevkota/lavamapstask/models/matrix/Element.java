package np.com.sagardevkota.lavamapstask.models.matrix;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by HP on 8/9/2016.
 */
public class Element implements Parcelable {
    Distance distance;
    Duration duration;
    String status;

    public String getStatus() {
        return status;
    }

    public Element setStatus(String status) {
        this.status = status;
        return this;
    }

    public Distance getDistance() {
        return distance;
    }

    public Element setDistance(Distance distance) {
        this.distance = distance;
        return this;
    }

    public Duration getDuration() {
        return duration;
    }

    public Element setDuration(Duration duration) {
        this.duration = duration;
        return this;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.distance, flags);
        dest.writeParcelable(this.duration, flags);
        dest.writeString(this.status);
    }

    public Element() {
    }

    protected Element(Parcel in) {
        this.distance = in.readParcelable(Distance.class.getClassLoader());
        this.duration = in.readParcelable(Duration.class.getClassLoader());
        this.status = in.readString();
    }

    public static final Parcelable.Creator<Element> CREATOR = new Parcelable.Creator<Element>() {
        @Override
        public Element createFromParcel(Parcel source) {
            return new Element(source);
        }

        @Override
        public Element[] newArray(int size) {
            return new Element[size];
        }
    };
}
