# Lava Protocols Assignment By Sagar Devkota
```
Introduction  
This document is about development project to be built on Android to be used by a
user who want to compare his daily tasks.Purpose of the document is to explain the requirement
the user and give more insights on the application should behave and look like.

Use Case  
The Android application is to be used by a person who would like to go to 7 locations (choose
any location within 10­40km from the starting point) from his house. In the casetheuser would
open up the application and key in his/her home address and then subsequently key in the 7
locations he/she would want to visit and search for the order of events based on 2 criterias as
follow,
   
● Travel time between locations with traffic included  
● Travel distance between locations    Show all possible matches for the above mentioned criteria.
 
Note : Please use Android and Google Maps API for the development.
The submission deadline is 3pm (Malaysia Time) 11 August 2016 (Thursday), 2 days.  
```


### Features
* Easy UI with endless pagination while getting list of places
* Material Design
* Design Patterns are properly followed, 
* Dependency injection is properly handled by using ButterKnife
* Annotation based network calls using retrofit which is the best HTTP library  

# Screens
![Screen 1](https://bytebucket.org/sagarda7/lavamapstask/raw/bf8537db40b4edf468de0050268ceb07cc4acf5f/screens/1.jpg "")

![Screen 2](https://bytebucket.org/sagarda7/lavamapstask/raw/bf8537db40b4edf468de0050268ceb07cc4acf5f/screens/2.jpg "")

# Download Tutorial Pdf
[Simple Pdf Tutorial](https://bitbucket.org/sagarda7/lavamapstask/raw/c41677e0ad5e08957df3c7839feb64c3de19e268/screens/LAVAPROTOCOLS_ASSIGNMENT_BY_SAGAR_DEVKOTA.pdf) 

# Download Sample App
[app-debug.apk](https://bitbucket.org/sagarda7/lavamapstask/raw/6f5a4a0e68502d210b3c8b6f70331192c290179d/app-debug.apk?raw=true) and install it to your mobile



### More Info about assignment
* Most of the functions and classes are well commented, due to lack of time I left some common methods
* I have tried to make a class be responsible to single purpose (Single Responsibility), eg. See any model class
* The coding may have some improvements and missing as well as bugs as this is just 2 days partial work. :) 

### Contact
Sagar Devkota
sagarda7@yahoo.com
+9779856032592
Skype: sagarda7